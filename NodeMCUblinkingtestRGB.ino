int highTime = 1000, lowTime = 100; // First, we define how long the light should be on (highTime) and how long the light should be off (lowTime) per blink in miliseconds (1000 = 1 sec)

void red() { // Next, we create a function for the color red. You can expariment or swap these after testing them. For now, we will map pin D5 to our function "red"
      digitalWrite(D5, HIGH), delay(highTime), digitalWrite(D5, LOW), delay(lowTime);
}
void green() { // We will repeat this process for the color green. As a note, these are "void" functions because unlike normal functions they don't return any data, they just do something.
      digitalWrite(D6, HIGH), delay(highTime), digitalWrite(D6, LOW), delay(lowTime);
}
void blue() { // Finally, we create a function for the color blue as well. 
      digitalWrite(D7, HIGH), delay(highTime), digitalWrite(D7, LOW), delay(lowTime);
}
void setup() { // This runs first to set up for our program, and only runs once. In this step, we will tun on the pins.
  pinMode(D5, OUTPUT), pinMode(D6, OUTPUT), pinMode(D7, OUTPUT); // Here we turn on pins D5, D6, and D7 for output.
      }

void loop() { // This loop will run over and over.
     red(),green(),blue(); // Here, we call our functions, red(), green(), and blue() one after another to test our blinking LED's. 
     delay(100); // The last thing we do is add a delay to create a pause between the blinking of our lights, in miliseconds. 
}